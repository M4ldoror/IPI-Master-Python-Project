#title           : menu.py
#description     : This script is a project for python class
#author          : Paco Garcia
#date            : 26/11/2021
#usage           : python menu.py  (csv files have to be in the same folder as the menu.py)
#==============================================================================

import csv
import os
from clint.textui import colored
from pyfiglet import Figlet
import numpy as np
import matplotlib.animation as animation
import matplotlib.pyplot as plt
from pytrends.request import TrendReq
import plotly.express as px
import seaborn as sns
import pandas as pd

# A nice Welcome to add a little bit of color :
def welcome(text):
    result = Figlet()
    return colored.cyan(result.renderText(text))

# Fonction ages : representing the plot biostat.csv
def ages():
    x = []
    y = []

#  Using csv lib for data parsing, delimiting by "," :
    with open('biostats.csv', 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')

#Appending rows in csv file  with for loop:
        for row in plots:
            x.append(row[0])
            y.append(int(row[2]))

    plt.bar(x, y, color='g', width=0.72, label="Age")
    plt.xlabel('Names')
    plt.ylabel('Ages')
    plt.title('Ages of different persons')
    plt.legend()
    plt.show()

def weather():
    x = []
    y = []

    with open('weather.csv', 'r') as csvfile:
        lines = csv.reader(csvfile, delimiter=',')
        for row in lines:
            x.append(row[0])
            y.append(int(row[1]))

    plt.plot(x, y, color='g', linestyle='dashed', marker='o', label="Weather Data")
    plt.xticks(rotation=25)
    plt.xlabel('Dates')
    plt.ylabel('Temperature(°C)')
    plt.title('Prévisions Météo', fontsize=20)
    plt.grid()
    plt.legend()
    plt.show()

# Fonction ages : representing data plot with csv file biostat.csv
def sante():
    x = []
    y = []

    with open('data-depression.csv', 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')
        for row in plots:
            # print(date.fromisoformat(row['annee']), float(row[('depression')])
            x.append(row[0])
            y.append(float(row[1]))

    plt.plot(x, y, color='b', linestyle='dashed',
             marker='o', label="Weather Data")
   # ax.axvspan(1.25, 1.55, facecolor='#2ca02c')
    plt.xticks(rotation=25)
    plt.xlabel('annee')
    plt.ylabel('depression en %')
    plt.title('Population étant depressive par age en %')
    plt.grid()
    plt.legend('H/F')
    plt.show()

# Fonction ages : represente le plot data biostat.csv
def etudiant():
    Sujets = []
    Scores = []

    with open('Etudiants.csv', 'r') as csvfile:
        lines = csv.reader(csvfile, delimiter=',')
        for row in lines:
            Sujets.append(row[0])
            Scores.append(int(row[1]))

    plt.pie(Scores, labels=Sujets, autopct='%.2f%%')
    plt.title('Marks of a Student', fontsize=20)
    plt.show()

def joy_division():
    # Fixing random state for reproducibility
    np.random.seed(19680801)

    # Create new Figure with black background
    fig = plt.figure(figsize=(8, 8), facecolor='black')

    # Add a subplot with no frame
    ax = plt.subplot(frameon=False)

    # Generate random data
    data = np.random.uniform(0, 1, (64, 75))
    X = np.linspace(-1, 1, data.shape[-1])
    G = 1.5 * np.exp(-4 * X ** 2)

    # Generate line plots
    lines = []
    for i in range(len(data)):
        # Small reduction of the X extents to get a cheap perspective effect
        xscale = 1 - i / 200.
        # Same for linewidth (thicker strokes on bottom)
        lw = 1.5 - i / 100.0
        line, = ax.plot(xscale * X, i + G * data[i], color="w", lw=lw)
        lines.append(line)

    # Set y limit (or first line is cropped because of thickness)
    ax.set_ylim(-1, 70)

    # No ticks
    ax.set_xticks([])
    ax.set_yticks([])

    # 2 part titles to get different font weights
    ax.text(0.5, 1.0, "MERCI ", transform=ax.transAxes,
            ha="right", va="bottom", color="w",
            family="sans-serif", fontweight="light", fontsize=16)
    ax.text(0.5, 1.0, "MR AYYOUB !", transform=ax.transAxes,
            ha="left", va="bottom", color="w",
            family="sans-serif", fontweight="bold", fontsize=16)

    def update(*args):
        # Shift all data to the right
        data[:, 1:] = data[:, :-1]

        # Fill-in new values
        data[:, 0] = np.random.uniform(0, 1, len(data))

        # Update data
        for i in range(len(data)):
            lines[i].set_ydata(i + G * data[i])

        # Return modified artists
        return lines

    # Construct the animation, using the update function as the animation director.
    anim = animation.FuncAnimation(fig, update, interval=10)
    plt.show()

def google_trend():
    import pandas as pd
    from pytrends.request import TrendReq
    import matplotlib.pyplot as plt

    pytrends = TrendReq(hl='en-US', tz=360)
    # build list of keywords
    kw_list = ["ai", "chicken", "space"]
    # store interest over time information in df

    pytrends.build_payload(kw_list, timeframe='2015-01-01 2021-01-01', geo='US')

    df = pytrends.interest_over_time()

    # display the top 20 rows in dataframe

    # plot all three trends in same chart
    plt.figure()
    plt.plot(df.index, df.ai, 'k*')
    plt.plot(df.index, df.chicken, 'r*')
    plt.plot(df.index, df.space, 'b*')
    plt.legend(['ai', 'chicken', 'space'])
    plt.show()

# Open in Chrome with webserver :
def google_trend2():
    #API request :
    pytrends = TrendReq(hl='en-US', tz=360)

    # list of keywords to get data :
    kw_list = ["machine learning"]  # list of keywords to get data

    #API payload :
    pytrends.build_payload(kw_list, cat=0, timeframe='today 12-m')
    data = pytrends.interest_over_time()
    data = data.reset_index()

    #Plot in webserver :
    fig = px.line(data, x="date", y=['machine learning'], title='Keyword Web Search Interest Over Time')
    fig.show()

def google_trend3():
    pytrends = TrendReq(hl='en-US', tz=360)
    pytrends.build_payload(['Coronavirus'], cat=0, timeframe='2020-02-01 2020-03-10', gprop='',
                           geo='US-{}'.format('NY'))
    df = pytrends.interest_over_time()
    sns.set()
    df['timestamp'] = pd.to_datetime(df.index)
    sns.lineplot(df['timestamp'], df['Coronavirus'])

    plt.title(
        "Normalized Searches for Coronavirus in NY (blue), MA (orange), and CA (green)".format('Coronavirus', 'NY'))
    plt.ylabel("Number of Searches")
    plt.xlabel("Date")
    plt.xticks(rotation=45)
    plt.show()

def anxiete():
    x = []
    y = []

    with open('vague_anxiete.csv', 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')

        for row in plots:
            x.append(row[0])
            y.append(int(row[1]))

    fig, ax = plt.subplots()
    ax.plot(x, y)

    ax.set(xlabel='Wave 2020', ylabel='% of anxiety',
           title='Percentage of people suffering from anxiety per COVID waves in France')
    ax.tick_params(axis='x', rotation=70)
    ax.grid()
    plt.show()

# Fonction principal faisant fonctionner le menu avec condition et input :
def main():
    while True:
        os.system("clear")
        print(welcome("\nWELCOME IPI"))
        print("-----------------------------------------------")
        print("\nQuelles données voulez vous choisir ?")
        print("\n-----------------------------------------------")
        print("""
        1 : Data from schools - Etudiants.csv
        2 : Health during COVID - data-depression.csv
        3 : Ages of patients - Ages.csv
        4 : Météo - weather.csv
        5 : Google search keyword search for : "ai, chicken, space" (with google trend)
        6 : Google search keyword search for : "machine learning" (with google trend, open in Google Chrome)
        7 : Google search Keyword search for : "Coronavirus" (with google trend)
        8 : Percentage of people suffering from anxiety during COVID waves - vague_anxiete.csv
        9 : Joy Division : Just for fun !
        0 : Exit"""
              )
        choice = input("\nEnter your choice, and hit [ENTER]: ")

        if choice == '1' :
            etudiant()
        elif choice == '2' :
            sante()
        elif choice == '3' :
            ages()
        elif choice == '4':
            weather()
        elif choice == '5':
            google_trend()
        elif choice == '6':
            google_trend2()
        elif choice == '7':
            google_trend3()
        elif choice == '8':
            anxiete()
        elif choice == '9':
            joy_division()
        elif choice == '0':
            exit()
        os.system("clear")

if __name__ == "__main__":
        main()

