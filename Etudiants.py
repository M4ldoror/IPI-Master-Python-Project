import matplotlib.pyplot as plt
import csv

Sujets = []
Scores = []

with open('Etudiants.csv', 'r') as csvfile:
	lines = csv.reader(csvfile, delimiter = ',')
	for row in lines:
		Sujets.append(row[0])
		Scores.append(int(row[1]))

plt.pie(Scores,labels = Sujets,autopct = '%.2f%%')
plt.title('Marks of a Student', fontsize = 20)
plt.show()
