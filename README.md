# IPI Master Python Project

![](IPI-Master-Python-Graph-Ebauches.jpg)


1. Clonez le repo en local sur votre machine pour lancer l'application :
   
   ```bash 
    git clone git@gitlab.com:M4ldoror/IPI-Master-Python-Project.git
   ```

2. Installer les librairies necessaires dans requirement.txt :
    
    ```bash
   virtualenv venv
   source venv/bin/activate
   pip install -r requirements.txt
    ```

3. Lancez l'application :
    
    ```python
    python menu.py
    ```
![](welcome.png)
